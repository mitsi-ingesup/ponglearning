import os
from random import sample

import numpy as np
from keras.models import load_model
from keras import Sequential
from keras.layers import Conv2D, Activation, MaxPooling2D, Flatten, Dense, Dropout
from scipy import misc
import matplotlib.pyplot as plt


BATCH_SIZE = 21
IMAGE_WIDTH = 28
IMAGE_HEIGHT = 22
EPOCHS = 500
TEST_SIZE = 5


def generate_label(index, size):
    label = []
    for i, v in enumerate(range(0, size)):
        label.append(1 if i == index else 0)
    return label


def dataset_to_numpy():
    new_data = np.empty((21, 22, 28, 3), int)
    labels_to_create = np.empty((21, 21), int)
    files = get_ordered_file_names()
    for i, image_name in enumerate(files):
        new_data[i] = misc.imread("/home/mitsi/dev/ponglearning/training_convolution/img" + "/" + str(image_name) +
                                  ".png")
        labels_to_create[i] = np.array(generate_label(i, len(files)))
    return new_data, labels_to_create


def get_ordered_file_names():
    files = os.listdir("/home/mitsi/dev/ponglearning/training_convolution/img")
    i: int = 0
    while i < len(files):
        files[i] = int(files[i].split(".png")[0])
        i += 1
    files = sorted(files)
    return files


def training(model_convo, image_dataset, label_dataset):
    model_convo.fit(image_dataset, label_dataset, batch_size=21, epochs=EPOCHS, verbose=1)
    return model_convo


def testing(model_to_use):
    for i in sample(list(range(0, 21)), 21):
        img = misc.imread("/home/mitsi/dev/ponglearning/training_convolution/img/"+str(i)+".png")
        print(i, i == model_to_use.predict(img.reshape((1, 22, 28, 3))).argmax())
        plt.imshow(img)
        # plt.show()


def create_model():
    convolutional_model = Sequential()

    convolutional_model.add(Conv2D(16, (4, 4), padding='same', input_shape=(22, 28, 3)))
    convolutional_model.add(Activation("relu"))
    convolutional_model.add(MaxPooling2D(pool_size=(2, 2)))

    convolutional_model.add(Conv2D(32, (3, 3), padding='same'))
    convolutional_model.add(Activation("relu"))
    convolutional_model.add(MaxPooling2D(pool_size=(2, 2)))

    convolutional_model.add(Conv2D(64, (3, 3), padding='same'))
    convolutional_model.add(Activation("relu"))
    convolutional_model.add(MaxPooling2D(pool_size=(2, 2)))

    convolutional_model.add(Flatten())
    convolutional_model.add(Dense(128))  # layer
    convolutional_model.add(Activation("relu"))
    convolutional_model.add(Dropout(0.5))
    convolutional_model.add(Dense(21))
    convolutional_model.add(Activation("softmax"))

    convolutional_model.compile(loss="categorical_crossentropy", optimizer="rmsprop", metrics=["accuracy"])
    return convolutional_model


if __name__ == "__main__":
    if os.path.isfile("/home/mitsi/dev/ponglearning/training_convolution/model_convolution.h5"):
        print("loading from file")
        model = load_model('/home/mitsi/dev/ponglearning/training_convolution/model_convolution.h5')
    else:
        model = create_model()
        data, labels = dataset_to_numpy()
        model = training(model, data, labels)

    testing(model)

    model.save('model_convolution.h5')
