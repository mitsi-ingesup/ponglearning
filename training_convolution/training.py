import datetime
import os
import random

import numpy as np
import tensorflow as tf
from PIL import Image
import matplotlib.pyplot as plt

batch_size = 64


def processImages(size, path):
    if not os.path.exists('new_train'):
        os.makedirs('new_train')

    timeNow = datetime.datetime.now()

    for file in os.listdir(path):
        img = Image.open(path + '/' + file)

        # Resizing
        img = img.resize(size)
        # Greyscale
        img = img.convert('L')

        img.save('new_train/' + file, 'JPEG')

    print("Temps écoulés : " + str(datetime.datetime.now() - timeNow))


def getInputs(path, list_input):
    timeNow = datetime.datetime.now()

    for file in os.listdir(path):
        img = Image.open(path + '/' + file)

        pixels = list(img.getdata())
        width, height = img.size
        pixels = [pixels[i * width:(i + 1) * width] for i in range(height)]

        list_input.append(pixels)

    print("Temps écoulés : " + str(datetime.datetime.now() - timeNow))


def getLabels(list_label, path):
    timeNow = datetime.datetime.now()

    for file in os.listdir(path):

        file_split = file.split('.')[0]

        if file_split == 'cat':
            file_split = [1, 0]
        elif file_split == 'dog':
            file_split = [0, 1]

        list_label.append(file_split)

    print("Temps écoulés : " + str(datetime.datetime.now() - timeNow))


processImages((50, 50), 'train')

list_input = []
list_label = []

getLabels(list_label, 'train')
getInputs('new_train', list_input)

X_train = np.array(list_input)
Y_train = np.array(list_label)

## No hidden layer
graph = tf.Graph()
with graph.as_default():
    X = tf.placeholder(tf.float32, [None, 50, 50])
    X_reshaped = tf.reshape(X, [-1, 2500])
    X_scaled = tf.nn.l2_normalize(X_reshaped, 1)

    W1 = tf.Variable(tf.truncated_normal([2500, 2]))
    b1 = tf.Variable(tf.zeros([2]))

    Y = tf.matmul(X_scaled, W1) + b1

    Y_ = tf.placeholder(tf.float32, [None, 2])

    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=Y_, logits=Y))

    optimizer = tf.train.GradientDescentOptimizer(0.5)
    train_step = optimizer.minimize(cross_entropy)

    is_correct = tf.equal(tf.argmax(Y, 1), tf.argmax(Y_, 1))
    accuracy = tf.reduce_mean(tf.cast(is_correct, tf.float32))


def get_minibatch(batch_size, samples_id, training_size):
    if batch_size > len(samples_id):
        new_samples = list(range(training_size - 1))
        random.shuffle(new_samples)
        samples_id.extend(new_samples)

    next_batch = samples_id[:batch_size]
    samples_id = samples_id[batch_size:]

    return next_batch, samples_id


nb_steps = X_train.shape[0] // batch_size
nb_epochs = 1
with tf.Session(graph=graph) as sess:
    sess.run(tf.global_variables_initializer())

    for epoch in range(nb_epochs):
        print("Epoch : " + str(epoch))
        ids = []
        for step in range(nb_steps):
            batch_ids, ids = get_minibatch(batch_size, ids, X_train.shape[0])
            train_data = {X: (X_train[batch_ids, :]), Y_: (Y_train[batch_ids, :])}

            sess.run(train_step, feed_dict=train_data)

            if step % 500 == 0:
                a, c = sess.run([accuracy, cross_entropy], feed_dict=train_data)
                print("Minibatch loss at epoch %d and step %d: %f" % (epoch, step, c))
                print("Minibatch accuracy: %f" % a)




    print(sess.run(Y, feed_dict={X: X_train[[0], :], Y: Y_train[[0], :]}))
    plt.imshow(X_train[0], cmap='gray')
    print(Y_train[0])
    plt.show()
    print(sess.run(Y, feed_dict={X: X_train[[len(X_train)-2], :], Y: Y_train[[len(X_train)-2], :]}))
    plt.imshow(X_train[len(X_train)-2], cmap='gray')
    print(Y_train[len(X_train)-2])
    plt.show()

