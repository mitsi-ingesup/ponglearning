# PongLearnig

Projet 2018-2019 de Ydays Machine learning

# Principe
Développer le jeu pong et permettre et utiliser le machine learning pour aboutir à une ia compétente.

Techno employées (à vue de nez):
  - Convolution pour la reconnaissance de pattern
  - LSTM pour la mouvement de la balle (ou autre RNN)
  - Deep Learning et son famuex mini batch

Sytème de développement:
- Kubuntu 18.04
- Tensorflow
- Python 3
- Tkinter ou Pygame pour le jeu -> gym openAI

Dépendance
- Python 3.6
- pip3 install gym
- pip3 install atari_py

Code basé sur:
 https://github.com/dhruvp/atari-pong 

