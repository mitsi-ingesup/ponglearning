from enum import Enum


class PaddleDirection(Enum):
    UP = 2
    DOWN = 3