import os

import numpy as np
from keras import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.optimizers import RMSprop


class NeuralNetwork:
    def __init__(self, checkpoints_dir):
        self.gamma = 0.25
        self.learning_rate = 0.0005
        self.observations_size = 15
        self.model = self.neural_net()
        self.checkpoint_file = os.path.join(checkpoints_dir, 'policy_network.ckpt')
        self.checkpoint_dir = checkpoints_dir

    @staticmethod
    def neural_net():
        model = Sequential()

        model.add(Dense(800))
        model.add(Activation("relu"))
        model.add(Dropout(0.5))

        # Output layer.
        model.add(Dense(2, kernel_initializer='lecun_uniform'))
        model.add(Activation('linear'))

        rms = RMSprop()
        model.compile(loss='mse', optimizer=rms)

        return model

    def load_checkpoint(self):
        print("Loading checkpoint...")
        self.model.load_weights(os.path.join(self.checkpoint_dir, "keras_model.h5"))
        return np.load(os.path.join(self.checkpoint_dir, "values.npy")).tolist()

    def save_checkpoint(self, plot_values):
        print("Saving checkpoint...")
        self.model.save(os.path.join(self.checkpoint_dir, "keras_model.h5"))
        np.save(os.path.join(self.checkpoint_dir, "values.npy"), plot_values)

    def compute_direction(self, observations):
        return self.model.predict(observations.reshape([1, -1]))[0].argmax()

    def train(self, state_delta_action_reward_tuples, nb_observation):
        if nb_observation < self.observations_size:
            return
        X_train = []
        y_train = []
        observation_delta_m, observation_delta_result_m, action_m, reward_m = zip(*state_delta_action_reward_tuples)
        batch_size = len(observation_delta_m)
        i = 0
        while i < batch_size:
            newQ = self.model.predict(observation_delta_result_m[i].reshape((1, len(observation_delta_m[i]))), batch_size=1)
            maxQ = np.max(newQ)
            y = np.zeros((1, 2))
            y[:] = newQ[:]
            if reward_m[i] == 0:
                update = (reward_m[i] + (self.gamma * maxQ))
            else:  # terminal state
                update = reward_m[i]
            y[0][action_m[i]] = update
            X_train.append(observation_delta_m[i].reshape(len(observation_delta_m[i]), ))
            y_train.append(y.reshape(2, ))
            i += 1

        self.model.fit(np.array(X_train), np.array(y_train), batch_size=batch_size, epochs=1, verbose=0)
