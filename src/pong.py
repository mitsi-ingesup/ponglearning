# FROM : https://github.com/mrahtz/tensorflow-rl-pong

# !/usr/bin/env python

"""
Train a Pong AI using policy gradient-based reinforcement learning.

Based on Andrej Karpathy's "Deep Reinforcement Learning: Pong from Pixels"
http://karpathy.github.io/2016/05/31/rl/
and the associated code
# https://gist.github.com/karpathy/a4166c7fe253700972fcbc77e4ea32c5
"""

import argparse

import gym
import matplotlib.pyplot as plt
import numpy as np
import scipy.misc
from keras.models import load_model
from nn import NeuralNetwork
from paddle import PaddleDirection

MAX_EPISODE = 1000
SAVE_EVERY_N_EPISODES = 10
GAMMA_FACTOR = 0.99
EPSILON = 0.40
BATCH_SIZE_EPISODE = 1
action_dict = {PaddleDirection.DOWN.value: 0, PaddleDirection.UP.value: 1}

saving_score_enemy = 0
need_to_save_score = True


def clean_input_ground(pixel_map):
    pixel_map_cropped = pixel_map[35:195]  # crop du terrain
    pixel_map_cropped = pixel_map_cropped[::2, ::2, 0]  # downsize the sample by factor 2
    clean_color(pixel_map_cropped)
    return pixel_map_cropped.astype(np.float).ravel()


def clean_color(pixel_map):
    pixel_map[pixel_map == 144] = 0  # erase background (background type 1)
    pixel_map[pixel_map == 109] = 0  # erase background (background type 2)
    pixel_map[pixel_map != 0] = 1  # everything else (paddles, ball) just set to 1


def clean_input_score(pixel_map, horizontal_start, horizontal_end):
    pixel_map_cropped = pixel_map[0:22]  # crop des scores
    pixel_map_cropped = crop_horizontally(pixel_map_cropped, horizontal_start, horizontal_end)
    clean_color(pixel_map_cropped)
    return pixel_map_cropped


def clean_input_my_score(pixel_map):
    return clean_input_score(pixel_map, 100, 128).astype(np.float).ravel()


def crop_horizontally(pixel_map, start, end):
    cropped_map = []
    i = 0
    while i < len(pixel_map):
        cropped_map.append(pixel_map[i][start:end])
        i += 1
    return np.array(cropped_map)


def clean_input_enemy_score(pixel_map, convolutionnal_model):
    global saving_score_enemy, need_to_save_score
    tmp = clean_input_score(pixel_map, 20, 48)
    # plt.imshow(tmp)
    # plt.show()
    if need_to_save_score and saving_score_enemy:
        scipy.misc.toimage(tmp, cmin=0.0, cmax=...).save('/home/mitsi/dev/ponglearning/src/other/img/' +
                                                         str(saving_score_enemy) + '.png')
        need_to_save_score = False
        prediction = convolutionnal_model.predict(tmp.reshape((1, 22, 28, 3)))
        print("Enemy points:", prediction.argmax())
    return tmp.astype(np.float).ravel()


def main(parsed_args, convolutional_model):
    env = gym.make('Pong-v0')
    network = NeuralNetwork(checkpoints_dir='checkpoints')
    start_index = 1
    plot_values = []

    if parsed_args.load_checkpoint:
        plot_values = network.load_checkpoint()
        start_index = len(plot_values)
        if parsed_args.show_plots:
            show_plots(plot_values)
            return

    batch_state_action_delta_reward_tuples = []

    for nb_episodes in range(start_index, MAX_EPISODE):
        print("Starting episode %d" % nb_episodes)
        last_observation = env.reset()
        last_observation = clean_input_ground(last_observation)
        action = env.action_space.sample()
        observation, _, _, _ = env.step(action)
        observation = clean_input_ground(observation)

        episode_reward_sum, nb_rounds = compute_episode(batch_state_action_delta_reward_tuples, env, last_observation,
                                                        network, observation, convolutional_model, nb_episodes)

        print("Episode %d finished after %d rounds" % (nb_episodes, nb_rounds))

        print("Reward total was %.3f" % episode_reward_sum)

        if nb_episodes % BATCH_SIZE_EPISODE == 0:
            observation_delta, observation_delta_result, actions, rewards = zip(*batch_state_action_delta_reward_tuples)
            indexes = np.random.randint(1, len(observation_delta), 64)
            batch_state_action_delta_reward_tuples = list(zip(reduce_values(observation_delta, indexes), reduce_values(observation_delta_result, indexes), reduce_values(actions, indexes), reduce_values(rewards, indexes)))
            network.train(batch_state_action_delta_reward_tuples, nb_episodes)
            batch_state_action_delta_reward_tuples = []

        plot_values.append((nb_episodes, episode_reward_sum))

    show_plots(plot_values)


def reduce_values(array, indexes):
    reduced_array = []
    for index in indexes:
        reduced_array.append(array[index])
    return reduced_array


def show_plots(plot_values):
    for v in plot_values:
        episode, sum_reward, average = v
        plt.plot(episode, sum_reward, 'bo', episode, average, 'ro')
    plt.title("Apprentissage")
    plt.ylabel('reward')
    plt.xlabel("episode")
    plt.show()


def compute_episode(batch_state_action_reward_tuples, env, last_observation,
                    pong_network, observation_ground, convolutional_model, nb_episode):
    global saving_score_enemy, need_to_save_score, EPSILON

    my_score = 0
    enemy_score = 0
    nb_rounds = 1
    nb_steps = 1
    episode_reward_sum = 0
    episode_done = False
    while not episode_done:
        if args.render:
            env.render()

        observation_delta = observation_ground - last_observation
        last_observation = observation_ground
        choice = choose_action(nb_episode, observation_delta, pong_network)
        action = PaddleDirection.UP.value if choice > 0 else PaddleDirection.DOWN.value

        observation_ground, reward, episode_done, info = env.step(action)
        clean_input_my_score(observation_ground)
        clean_input_enemy_score(observation_ground, convolutional_model)
        observation_ground = clean_input_ground(observation_ground)

        episode_reward_sum += reward
        nb_steps += 1

        batch_state_action_reward_tuples.append((observation_delta, observation_ground - last_observation, action_dict[action], reward))

        if reward == -1:
            saving_score_enemy += 1 % 20
            need_to_save_score = True
            enemy_score += 1

        elif reward == +1:
            print("Round %d: %d time steps; won!" % (nb_rounds, nb_steps))
            my_score += 1 % 20

        if reward != 0:
            nb_rounds += 1
            nb_steps = 0
    need_to_save_score = False
    return episode_reward_sum, nb_rounds


def choose_action(nb_episode, observation_delta, pong_network):
    global EPSILON

    if nb_episode < pong_network.observations_size:
        choice = np.random.uniform(low=-1.0, high=1.0)
    elif np.random.uniform() < EPSILON:
        choice = np.random.uniform(low=-1.0, high=1.0)
        EPSILON -= 0.001
    else:
        choice = pong_network.compute_direction(observation_delta)
    return choice


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--load_checkpoint', action='store_true')
    parser.add_argument('--render', action='store_true')
    parser.add_argument('--show_plots', action='store_true')
    args = parser.parse_args()
    args.show_plots = True
    args.render = True
    model = load_model('/home/mitsi/dev/ponglearning/src/precompiled/model_convolution.h5')
    model.compile(loss="categorical_crossentropy", optimizer="rmsprop", metrics=["accuracy"])
    main(args, model)
